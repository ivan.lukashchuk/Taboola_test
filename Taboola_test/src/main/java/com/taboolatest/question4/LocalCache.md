# The cache should have the following functionality:

## Retrieving

1. Creating a cache instance with loader function
2. Getting a cache entry by key. A blocking operation loads an entry if it is absent in the cache or returns an existing one if it is already in the cache.
3. Getting an optional cache entry by key. A non-blocking operation returns a Some value if it is already loaded or EMPTY if it is not loaded or loading (starts loading if not loading).
4. Getting a Future with the cache entry by key. A non-blocking operation that returns Future completed with the entry if loaded or not completed Future if the entry is not already loaded, and the Future will be completed when the entry is loaded.
5. Getting the size of a cache

## Modifying

1. Removing an entry by key
2. Reloading an entry by Key
3. Putting an entry manually

## Configuration

1. Time to live setting (after last load or last access)
2. Max size of a cache
3. Max weight of a cache (additionally requires providing a size measurement mechanism for entry)
4. Using weak keys
5. Schedule auto-reloading
6. Setting for eviction policy (LRU, LFU, MRU)

## Eventing

1. There could be a callback of evicting operating with a reason of the entry evicting.
2. Analytics gathering of the cache usage (reading, loading, updating, removing)

# Implementation notes

Could be used a ConcurrentMap as a key, value cache storage, and Queue for supporting cache evicting policies. To deal with concurrency, it is needed to use ReadLock on rearing operations and WriteLock on modifying operations to allow reading and writing in parallel.