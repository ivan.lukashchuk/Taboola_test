package com.taboolatest.question3;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class WordsCounter {

    private Map<String, AtomicInteger> wordsCount = new ConcurrentHashMap<>();

    public static void main(String[] args) {

        WordsCounter wc = new WordsCounter();

        // load text files in parallel
        wc.load("filel.txt", "file2.txt", "file3.txt");

        // display words statistics

        wc.displayStatus();

    }

    public void load(String... s) {

        Arrays.stream(s).parallel().forEach(filePath -> {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
                bufferedReader.lines().forEach(line -> {
                    Arrays.stream(line.split(" ")).forEach(word -> {
                        wordsCount.computeIfAbsent(word, k -> new AtomicInteger()).incrementAndGet();
                    });
                });
            } catch (IOException ex) {
                log.error("Error during reading a file", ex);
            }
        });

    }

    public void displayStatus() {
        System.out.println(getStatusString());
    }

    public String getStatusString() {
        StringBuilder statusBuilder = new StringBuilder();
        wordsCount.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .forEach(e -> statusBuilder.append(e.getKey()).append(" ").append(e.getValue().get()).append("\n"));

        statusBuilder.append("\n** total: ")
                .append(wordsCount.values().stream().map(AtomicInteger::get).reduce(0, Integer::sum));

        return statusBuilder.toString();

    }


}