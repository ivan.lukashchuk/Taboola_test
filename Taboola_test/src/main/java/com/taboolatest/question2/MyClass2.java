package com.taboolatest.question2;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public final class MyClass2 {
    private final Date time;
    private final String name;
    private final List<Long> numbers;
    private final List<String> strings;

    public MyClass2(Date time, String name, List<Long> numbers, List<String> strings) {
        this.time = new Date(time.getTime());
        this.name = name;
        this.numbers = List.copyOf(numbers);
        this.strings = List.copyOf(strings);
    }

    public boolean equalsByName(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        MyClass2 myClass2 = (MyClass2) obj;
        return Objects.equals(name, myClass2.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyClass2 myClass2 = (MyClass2) o;
        return Objects.equals(time, myClass2.time) && Objects.equals(name, myClass2.name) && Objects.equals(numbers, myClass2.numbers) && Objects.equals(strings, myClass2.strings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, name, numbers, strings);
    }

    public String toString() {
        StringBuilder out = new StringBuilder(name);
        for (long item : numbers) {
            out.append(" ").append(item);
        }

        return out.toString();
    }

    public List<String> removeString(String str) {
        return strings.stream().filter(s -> Objects.equals(s, str)).toList();
    }

    public boolean containsNumber(long number) {
        return numbers.contains(number);
    }

    public boolean isHistoric() {
        return time.before(new Date());
    }

    public Date getTime() {
        return new Date(time.getTime());
    }

    public String getName() {
        return name;
    }

    public List<Long> getNumbers() {
        return List.copyOf(numbers);
    }

    public List<String> getStrings() {
        return List.copyOf(strings);
    }
}