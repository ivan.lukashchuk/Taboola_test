package com.taboolatest.question1a.util;

import com.taboolatest.question1a.model.Digits;

import java.util.List;

public class Util {

    public static String parseDigitsFrom3Lines(List<String> lines, int length, String typoIndicationSymbol) {
        validateInputLines(lines, length);

        char[] line1 = lines.get(0).toCharArray();
        char[] line2 = lines.get(1).toCharArray();
        char[] line3 = lines.get(2).toCharArray();
        StringBuilder result = new StringBuilder();
        for (var i = 0; i <= (length - 1) * 3; i = i + 3) {
            int digit = getBitByPosition(line1[i + 1], 0)
                    + getBitByPosition(line2[i + 2], 1)
                    + getBitByPosition(line3[i + 2], 2)
                    + getBitByPosition(line3[i + 1], 3)
                    + getBitByPosition(line3[i], 4)
                    + getBitByPosition(line2[i], 5)
                    + getBitByPosition(line2[i + 1], 6);
            result.append(Digits.DIGIT_MAP.getOrDefault(digit, typoIndicationSymbol));
        }
        return result.toString();
    }



    private static void validateInputLines(List<String> lines, int length) {
        if (lines.size() < 3)
            throw new RuntimeException("Not enough lines, should be at least 3. The input has lines count: " + lines.size());
        lines.forEach(s -> {
            if (s == null || s.length() < length * 3) throw new RuntimeException("Line should contain at least 27 characters. Line: " + s);
        });
    }

    public static int getBitByPosition(char c, int position) {
        return (c != ' ') ? 1 << position : 0;
    }
}
