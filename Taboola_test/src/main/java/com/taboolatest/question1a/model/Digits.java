package com.taboolatest.question1a.model;

import java.util.Map;

public class Digits {
    /*
    7 segment digit could be encoded by bit mask of the segments:
      0
      _
   5 |_| 1
   4 |_| 2

      3

      and the central '_' is 6th bit

     */

    public final static int ONE = (1 << 1) + (1 << 2);
    public final static int TWO = 1 + (1 << 1) + (1 << 6) + (1 << 4) + (1 << 3);
    public final static int THREE = 1 + (1 << 1) + (1 << 2) + (1 << 6) + (1 << 3);
    public final static int FOUR = (1 << 1) + (1 << 5) + (1 << 6) + (1 << 2);
    public final static int FIVE = 1 + (1 << 5) + (1 << 6) + (1 << 2) + (1 << 3);
    public final static int SIX = 1 + (1 << 5) + (1 << 6) + (1 << 2) + (1 << 3) + (1 << 4);
    public final static int SEVEN = 1 + (1 << 1) + (1 << 2);
    public final static int EIGHT = 1 + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5) + (1 << 6);
    public final static int NINE = 1 + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 5) + (1 << 6);
    public final static int ZERO = 1 + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5);

    public final static Map<Integer, String> DIGIT_MAP = Map.of(
            ONE,   "1",
            TWO,   "2",
            THREE, "3",
            FOUR,  "4",
            FIVE,  "5",
            SIX,   "6",
            SEVEN, "7",
            EIGHT, "8",
            NINE,  "9",
            ZERO, "0"
    );
}
