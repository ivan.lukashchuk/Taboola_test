package com.taboolatest.question1a;

import com.taboolatest.question1a.services.impl.InvoiceServiceImpl;

public class Q1Main {
    public static void main(String[] args) {
        InvoiceServiceImpl fileService = new InvoiceServiceImpl();
        fileService.processInvoicesFile(args[0], args[1]);
    }
}