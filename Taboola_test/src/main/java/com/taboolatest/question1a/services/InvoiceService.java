package com.taboolatest.question1a.services;

public interface InvoiceService {

    void processInvoicesFile(String inputFileLocation, String outputFileLocation);
}
