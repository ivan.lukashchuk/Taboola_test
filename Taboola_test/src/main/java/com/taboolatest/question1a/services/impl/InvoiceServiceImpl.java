package com.taboolatest.question1a.services.impl;

import com.taboolatest.question1a.services.InvoiceService;
import com.taboolatest.question1a.util.Util;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
public class InvoiceServiceImpl implements InvoiceService {

    public static final int LENGTH = 9;
    public static final String TYPO_INDICATION_SYMBOL = "?";

    public static final String ILLEGAL_INDICATION = " ILLEGAL";

    @Override
    public void processInvoicesFile(String inputFileLocation, String outputFileLocation) {
        createFileIfDoesNotExist(outputFileLocation);

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFileLocation));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFileLocation))) {

            List<String> threeLines = read3Lines(bufferedReader);

            while (threeLines.stream().noneMatch(Objects::isNull)) {
                String invoice = Util.parseDigitsFrom3Lines(threeLines, LENGTH, TYPO_INDICATION_SYMBOL);

                invoice = enrichIfIllegal(invoice);

                bufferedWriter.write(invoice + "\n");

                bufferedReader.readLine(); //skip 4th empty line
                threeLines = read3Lines(bufferedReader);
            }
        } catch (IOException ex) {
            log.error("Error during reading a file", ex);
        }
    }

    private String enrichIfIllegal(String invoice) {
        if (invoice.contains(TYPO_INDICATION_SYMBOL)) {
            invoice = invoice + ILLEGAL_INDICATION;
        }
        return invoice;
    }

    private void createFileIfDoesNotExist(String outputFileLocation) {
        try {
            log.info(outputFileLocation + (new File(outputFileLocation).createNewFile() ? " file created" : " file already exists"));
        } catch (IOException ex) {
            log.error("Could not create a file", ex);
        }
    }

    private List<String> read3Lines(BufferedReader bufferedReader) throws IOException {
        return Arrays.asList(bufferedReader.readLine(), bufferedReader.readLine(), bufferedReader.readLine());
    }


}
