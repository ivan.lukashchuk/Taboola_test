package com.taboolatest.question1a.services.impl;

import junitx.framework.FileAssert;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

@Slf4j
class InvoiceServiceImplTest {

    private final String testOutputFilePath = ClassLoader.getSystemResource("").getPath() + "test_output_Q1a.txt";

    @BeforeEach
    void setUp() {
        //noinspection ResultOfMethodCallIgnored
        new File(testOutputFilePath).delete();
    }

    private String getPathToFile(String name) {
        return ClassLoader.getSystemResource(name).getPath();
    }

    @Test
    void processInvoicesFileQ1a() {
        InvoiceServiceImpl service = new InvoiceServiceImpl();
        String inputFilePath = getPathToFile("question1a/input_Q1a.txt");
        String expectedOutputFilePath = getPathToFile("question1a/output_Q1a.txt");

        service.processInvoicesFile(inputFilePath, testOutputFilePath);
        FileAssert.assertEquals(new File(expectedOutputFilePath), new File(testOutputFilePath));
    }

    @Test
    void processInvoicesFileQ1b() {
        InvoiceServiceImpl service = new InvoiceServiceImpl();
        String inputFilePath = getPathToFile("question1a/input_Q1b.txt");
        String expectedOutputFilePath = getPathToFile("question1a/output_Q1b.txt");

        service.processInvoicesFile(inputFilePath, testOutputFilePath);
        FileAssert.assertEquals(new File(expectedOutputFilePath), new File(testOutputFilePath));
    }

}