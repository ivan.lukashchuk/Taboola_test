package com.taboolatest.question1a.util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UtilTest {

    @Test
    void parseInvoiceFrom3Lines() {
        List<String> lines = Arrays.asList(
                " _  _  _        _     _  _ ",
                "|_ | || |  ||_| _|  ||_ |_ ",
                "|_||_||_|  |  | _|  | _| _|"
        );

        assertEquals("600143155", Util.parseDigitsFrom3Lines(lines, 9, "?"));
    }

    @Test
    void parseInvoiceFrom3LinesAllDigits() {
        List<String> lines = Arrays.asList(
                "    _  _     _  _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|| |",
                "  ||_  _|  | _||_|  ||_| _||_|"
        );

        assertEquals("1234567890", Util.parseDigitsFrom3Lines(lines, 10, "?"));
    }

    @Test
    void parseInvoiceFrom3LinesWithTypo() {
        List<String> lines = Arrays.asList(
                " _  _  _              _  _ ",
                "|_ | || |  ||_| _|  ||_ |_ ",
                "|_||_||_|  |  | _|  | _| _|"
        );

        assertEquals("60014?155", Util.parseDigitsFrom3Lines(lines, 9, "?"));
    }

    @Test
    void parseInvoiceFrom3LinesWithAllTypo() {
        List<String> lines = Arrays.asList(
                " _  _    ",
                "|_ | || |",
                "|_  _||_|"
        );

        assertEquals("!!!", Util.parseDigitsFrom3Lines(lines, 3, "!"));
    }

    @Test
    void parseInvoiceFrom3LinesWithLessLines() {
        assertThrows(RuntimeException.class,
                () -> {
                    List<String> lines = Arrays.asList(
                            " _  _    ",
                            "|_  _||_|"
                    );
                    Util.parseDigitsFrom3Lines(lines, 3, "?");
                });
    }

    @Test
    void parseInvoiceFrom3LinesWithLessChars() {
        assertThrows(RuntimeException.class,
                () -> {
                    List<String> lines = Arrays.asList(
                            "  _  _ ",
                            "| _| _|",
                            "||_  _"
                    );
                    Util.parseDigitsFrom3Lines(lines, 3, "?");
                });
    }

    @Test
    void getBitByPosition() {
        assertEquals(0, Util.getBitByPosition(' ', 0));
        assertEquals(0, Util.getBitByPosition(' ', 1));
        assertEquals(0, Util.getBitByPosition(' ', 8));

        assertEquals(1, Util.getBitByPosition('_', 0));
        assertEquals(2, Util.getBitByPosition('_', 1));
        assertEquals(16, Util.getBitByPosition('|', 4));
        assertEquals(128, Util.getBitByPosition('|', 7));
    }
}